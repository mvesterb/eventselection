FROM atlas/analysisbase:21.2.108
COPY --chown=atlas . /code/src
RUN sudo mkdir /code/build && \
    sudo chown -R atlas /code/build && \
    cd /code/build && \
    source /release_setup.sh && \
    cmake /code/src && \
    make -j4

